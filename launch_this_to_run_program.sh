#!/bin/bash

# the first python call is to the discord scraper that adds images to folders as they come in. the next three calls occur at the same time calling each python slideshow script independently and opening them each in their own window
# as it stands, one must manually reposition the new windows on a monitor. this is not a scriptable function, as it depends entirely on the window manager that is being used.
python3 discord.py & python3 ./Discord\ Scrapes/PatriotsSoapbox/patriots-graphics-design/graphics_design_slideshow.py & python3 ./Discord\ Scrapes/PatriotsSoapbox/memes/memes_slideshow.py & python3 ./Discord\ Scrapes/PatriotsSoapbox/redpill-memes/redpill_slideshow.py
