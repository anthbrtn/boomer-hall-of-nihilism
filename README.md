# Boomer Hall of Nihilism

## tl;dr
Open `launch_this_to_run_program.sh` in a terminal, and then move each of the 3 windows that opens to its own monitor.

## Introduction
This program is the basis of the 'Boomer Hall of Nihilism', a three-window setup that pulls images posted to one of three channels on the PatriotsSoapBox Discord server and displays them at random in a window at 800 x 640 resolution. Images are displayed at their scaled resolution to fit in this window; as such, if a previous image is larger than the next image called to display, a flickering rendering of the previous image will remain in the background. Images change every 3 seconds, and a small pan and zoom function (with a random direction and speed) occurs each time a new image is loaded.

## Requirements

This program is a bash script that calls three separate python3 functions, so it will require an installation of python3. It uses the external python3 packages `argparse`, `requirements`, and `pyglet`, which can all be downloaded from the Python Package Index by running `pip3 install [package_name]` on your terminal. It also requires the following modules that almost certainly came with your install of python3: time, json, os, sys, unicodedata, mimetypes, and random. If they are not installed, you can also install them from the PPI.
Access to the Discord API is based off of token authentication, which means that you'll need your own Discord account. [This](https://discordhelp.net/discord-token) website contains information on how to acquire this. The `discord.json_sample` file **has** to be changed in two ways: one, it does not contain a token, so you'll have to add your own. The second is that you should rename it to `discord.json`.

## Components
The `discord.json` file contains the configuration for the scraping tool, including the account authorization token for the API call as well as the directive to only download images as opposed to text posts as well.

The `discord.py` file is what does most of the scraping work: it makes the API call, downloads any files that do not already exist, and places them into a directory labelled `./ds_scraper_folder/[server_name]/[channel_name]`. As long as this script is running, then each image that is posted in a channel will be downloaded, because the API call for a particular image is made immediately after the last one has finished. If there is no new image posted, then the script waits (at whatever the value of `timems - 1420070400000` is; I'm not totally sure what this is) and then makes another call to check for images.

Each of the above-mentioned `[channel_name]` folders contain all images that are downloaded from the particular server's channel. They also contain the `[name_of_channel]_slideshow.py` script, which launches the slideshow as it is configured to display images from the folder. Since the `*_slideshow.py` script rereads the present working directory upon each image update (so every 3 seconds), the images that are pulled in real time by `discord.py` are added to the pool of potential images to be displayed next. With modifications, the slideshow scripts could display the images only as they come in, but in the instance of the `patrios-graphics-design` channel and the `redpill-memes` channel, they're not updated enough for this to have the intended aesthetic effect, so I've chosen to do a random image display instead.

## Credits
Credit goes to Jack Wilson for the conceptual discussions, [cgoldberg](https://github.com/cgoldberg/py-slideshow) for his py-slideshow script, and [Dracovian](https://github.com/Dracovian/Discord-Scraper) for his discord-scraper script. I've kept cgoldberg and Dracovian's licensing headers in each of their scripts because I did not program them, only tweaked them to suit my own needs.
